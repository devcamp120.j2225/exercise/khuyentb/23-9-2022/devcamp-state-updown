import { Component } from "react"
import "bootstrap/dist/css/bootstrap.min.css"

class UpAndDownBtn extends Component { 
    constructor (props) {
        super(props);

        this.state = {
            count: 0
        }
    }

    clickChangeUpHandler = () => {
        this.setState ({
            count: this.state.count + 1
        })
    }

    clickChangeDownHandler = () => {
        this.setState ({
            count: this.state.count - 1
        })
    }

    render () {
        return (
            <div className="container text-center mt-5" style={{backgroundColor: "#CCF1FF", padding: "10px", width: "600px", border: "7px solid"}}>
                <p>Count: {this.state.count}</p>             
                <button className="btn btn-success col-3" onClick={this.clickChangeUpHandler}>Btn Up</button>
                <button className="btn btn-danger col-3" onClick={this.clickChangeDownHandler}>Btn Down</button>                            
            </div>
        )
    }
}

export default UpAndDownBtn ;